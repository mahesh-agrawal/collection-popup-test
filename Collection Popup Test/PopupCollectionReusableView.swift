//
//  PopupCollectionReusableView.swift
//  Collection Popup Test
//
//  Created by Mahesh Agrawal on 11/2/16.
//  Copyright © 2016 Mahesh Agrawal. All rights reserved.
//

import UIKit

class PopupCollectionReusableView: UICollectionReusableView {
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var btnClose: UIButton!
}
