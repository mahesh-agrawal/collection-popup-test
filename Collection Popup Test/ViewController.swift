//
//  ViewController.swift
//  Collection Popup Test
//
//  Created by Mahesh Agrawal on 11/2/16.
//  Copyright © 2016 Mahesh Agrawal. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    var messageText: String = ""
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- Button Actions -

    @IBAction func showAction(sender: UIBarButtonItem) {
        messageText = "This will be the message for the mode show where we are assigning this text to show the message"
        self.collectionView.performBatchUpdates({ 
            self.collectionView.reloadData()
        }) { (finished: Bool) in
            
        }
    }
    
    @IBAction func closeAction(sender: UIButton) {
        messageText = ""
        /*self.collectionView.performBatchUpdates({
            self.collectionView.reloadSections(NSIndexSet(index: 0))
        }) { (finished: Bool) in
            
        }*/
        UIView.animateWithDuration(0.3) { 
            self.collectionView.collectionViewLayout.invalidateLayout()
        }
    }
    
    // MARK:- UICollectionViewDataSource -
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: UICollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath)
        
        return cell
    }
    
    // MARK:- UICollectionViewDelegate -
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        if kind == UICollectionElementKindSectionHeader {
            let view: PopupCollectionReusableView = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: "Header", forIndexPath: indexPath) as! PopupCollectionReusableView
            view.layoutIfNeeded()
            view.btnClose.superview?.alpha = 0
            
            view.lblText.superview?.layer.cornerRadius = 5.0
            view.lblText.superview?.layer.masksToBounds = true
            
            view.btnClose.backgroundColor = UIColor.whiteColor()
            view.btnClose.layer.cornerRadius = view.btnClose.frame.size.width / 2.0
            view.btnClose.layer.borderColor = UIColor.blackColor().CGColor
            view.btnClose.layer.borderWidth = 1.0
            
            view.lblText.text = messageText
            view.btnClose.addTarget(self, action: #selector(closeAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            
            UIView.animateWithDuration(0.3, animations: { 
                view.btnClose.superview?.alpha = 1.0
            })
            
            return view
        } else {
            return UICollectionReusableView(frame: CGRectZero)
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if messageText.characters.count > 0 {
            let height = NSString(format: messageText).boundingRectWithSize(CGSizeMake(290.0, 0), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: UIFont(name: "HelveticaNeue", size: 15.0)!], context: nil)
            return CGSizeMake(UIScreen.mainScreen().bounds.size.width - 30.0, height.size.height + 20.0)
        } else {
            return CGSizeZero
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake((UIScreen.mainScreen().bounds.size.width / 2.0), UIScreen.mainScreen().bounds.size.height * (170.0/568.0))
    }
}

